﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneySystem : MonoBehaviour {

    public static int MainMoney;
    public Text MoneyText;

	// Use this for initialization
	void Start () {       
        MainMoney = PlayerPrefs.GetInt("money");
        MoneyText.text = MainMoney.ToString();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void AddMoney(int value)
    {
        MainMoney += value;
        PlayerPrefs.SetInt("money", MainMoney);
        MoneyText.text = MainMoney.ToString();
    }
}
