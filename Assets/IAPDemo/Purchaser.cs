﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;
using UnityEngine.UI;

public class Purchaser : MonoBehaviour, IStoreListener
{
    private static IStoreController m_StoreController;          // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

    public static string G5 = "com.horusent.bs.gold_5";
    public static string G10 = "com.horusent.bs.gold_10";
    public static string G50 = "com.horusent.bs.gold_50";
    public static string G100 = "com.horusent.bs.gold_100";

    public PurchaseProduct[] ConsumableProducts =
        {
            new PurchaseProduct() {ProductId = G5, AppleName = G5, GooglePlayName = G5 },
            new PurchaseProduct() {ProductId = G10, AppleName = G10, GooglePlayName = G10 },
            new PurchaseProduct() {ProductId = G50, AppleName = G50, GooglePlayName = G50 },
            new PurchaseProduct() {ProductId = G100, AppleName = G100, GooglePlayName = G100 }
        };

    void Awake()
    {
        ButtonGetAccessToken();
    }

    void Start()
    {

        // buil unity purchase
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        AddProduct(builder, ProductType.Consumable);
        UnityPurchasing.Initialize(this, builder);

        // load receipt fail respone from server
        Debug.LogError("DEVICEID: " + SystemInfo.deviceUniqueIdentifier);
    }

    private void AddProduct(ConfigurationBuilder builder, ProductType productType)
    {

        int length = ConsumableProducts.Length;

        for (int i = 0; i < length; i++)
        {
            IDs ids = new IDs();
            PurchaseProduct product = ConsumableProducts[i];

            // AppleAppStore
            if (!String.IsNullOrEmpty(product.AppleName))
            {
                ids.Add(product.AppleName, AppleAppStore.Name);
            }
            // GooglePlay
            if (!String.IsNullOrEmpty(product.GooglePlayName))
            {
                ids.Add(product.GooglePlayName, GooglePlay.Name);
            }

            builder.AddProduct(product.ProductId, productType, ids);
        }
    }


    public void BuyG5()
    {
        BuyProductID(G5);
        currentID = 4;
    }

    public void BuyG10()
    {
        BuyProductID(G10);
        currentID = 5;
    }

    public void BuyG50()
    {
        BuyProductID(G50);
    }

    public void BuyG100()
    {
        BuyProductID(G100);
    }

    void BuyProductID(string productId)
    {

        if (IsInitialized())
        {
            if (!hasNetworkConnection())
            {
                return;
            }

            Product product = m_StoreController.products.WithID(productId);

            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Buy Product"));
                m_StoreController.InitiatePurchase(product);
            }
            else
            {
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        else
        {
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }


    public void RestorePurchases()
    {
        // If Purchasing has not yet been set up ...
        if (!IsInitialized())
        {
            // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        // If we are running on an Apple device ... 
        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            // ... begin restoring purchases
            Debug.Log("RestorePurchases started ...");

            // Fetch the Apple store-specific subsystem.
            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
            // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
            apple.RestoreTransactions((result) =>
            {
                // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                // no purchases are available to be restored.
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        // Otherwise ...
        else
        {
            // We are not running on an Apple device. No work is necessary to restore purchases.
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }


    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        //Debug.Log("OnInitialized: PASS");
        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        int length = ConsumableProducts.Length;
        for (int i = 0; i < length; i++)
        {
            PurchaseProduct product = ConsumableProducts[i];
            if (String.Equals(args.purchasedProduct.definition.id, product.ProductId, StringComparison.Ordinal))
            {
                //ValidateOnServer(args.purchasedProduct);
                //Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

                HttpResponse<VerifyIAPResponseData> verify = VerifyIAP(args.purchasedProduct);
                switch (verify.ReturnCode)
                {
                    case ReturnCode.OK:
                        //show popup voi mail reward tra ve
                        MailRewardIAP mail = verify.Data.mailReward;
                        // do something
                        Debug.Log(string.Format("Your mail reward object id is {0}", mail.MailObjectID));
                        break;
                    case ReturnCode.ProductPurchased:
                    case ReturnCode.AccountIsInvalid:
                    case ReturnCode.InvalidReceipt:
                        Debug.Log(verify.Message);
                        break;
                }

            }
        }
        pendingProduct = args.purchasedProduct;
        return PurchaseProcessingResult.Pending;
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }

    private bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    #region Operation
    #region UI Listener

    public void ButtonGetAccessToken()
    {
        textToken.text = GetAccessToken(deviceId, facebookId);
    }

    public void ButtonGetMailPurchase()
    {
        ClaimMailIAP(mailId);
    }

    public void OnSetFacebookId(string id)
    {
        facebookId = id;
    }

    public void OnSetIP(string ip)
    {
        baseAddress = ip;
    }

    public void OnSetPort(string p)
    {
        port = p;
    }

    public void OnSetDeviceId(string Id)
    {
        deviceId = Id;
    }

    public void OnSetMailId(string id)
    {
        mailId = id;
    }

    #endregion

    Product pendingProduct;
    public void ClaimMailIAP(string mailObjectId)
    {
        HttpResponse<ClaimRewardsResponseData> apiResponse = ClaimMailPurchase(mailObjectId);

        switch (apiResponse.ReturnCode)
        {
            case ReturnCode.OK:
                //do phần thưởng là random số lượng nên là trả về list số lượng đã được add vào data base do server roll ra,
                //show ra cho user thấy là đc bn, update database client
                ClaimRewardsResponseData data = apiResponse.Data;
                Debug.Log(data.QuantityReward);
                m_StoreController.ConfirmPendingPurchase(pendingProduct);
                break;
            case ReturnCode.MailRewardWasClaimed:
                m_StoreController.ConfirmPendingPurchase(pendingProduct);
                break;
            case ReturnCode.MailRewardInvalid:            
            case ReturnCode.AccountIsInvalid:
                Debug.Log(apiResponse.Message);
                break;
        }
    }

    #endregion

    #region API
    public static string GetAccessToken(string deviceID, string facebookID)
    {
        if (string.IsNullOrEmpty(deviceID))
            deviceID = SystemInfo.deviceUniqueIdentifier;
        string token = GetApi(string.Format("{0}:{1}/{2}", baseAddress, port, string.Format(APIList.GetToken, deviceID, facebookID))).ToString();
        AccessToken = token;
        Debug.LogError(token);
        return token;
    }

    int currentID;
    public HttpResponse<VerifyIAPResponseData> VerifyIAP(Product product)
    {
        if (string.IsNullOrEmpty(AccessToken))
            GetAccessToken(deviceId, facebookId);
        HttpResponse<VerifyIAPRequestData> request = new HttpResponse<VerifyIAPRequestData>();
        request.Data = new VerifyIAPRequestData()
        {
            receipt = JsonConvert.DeserializeObject<ReceiptClient>(product.receipt)
        };
        request.Data.ItemID = currentID;
        string responseJson = PostApi(request, string.Format("{0}:{1}/{2}", baseAddress, port, APIList.VerifyIAP),
            authourName, AccessToken).ToString();
        
        return JsonConvert.DeserializeObject<HttpResponse<VerifyIAPResponseData>>(responseJson);
    }

    public HttpResponse<ClaimRewardsResponseData> ClaimMailPurchase(string mailObjectId)
    {
        HttpResponse<ClaimMailRewardRequestData> request = new HttpResponse<ClaimMailRewardRequestData>();
        request.Data = new ClaimMailRewardRequestData()
        {
            MailObjectID = mailObjectId
        };

        string responseJson = PostApi(request, string.Format("{0}:{1}/{2}", baseAddress, port, APIList.ClaimMailPurchase),
            authourName, AccessToken).ToString();
        return JsonConvert.DeserializeObject<HttpResponse<ClaimRewardsResponseData>>(responseJson);
    }

    #endregion


    #region API Controller
    public static WebClient client;
    static string _AccessToken;
    static string AccessToken
    {
        get
        {
            if (string.IsNullOrEmpty(_AccessToken))
                _AccessToken = PlayerPrefs.GetString("AccessToken", "");
            if (string.IsNullOrEmpty(_AccessToken))
                GetAccessToken(deviceId, facebookId);
        return _AccessToken;
        }
        set
        {
            _AccessToken = value;
            PlayerPrefs.SetString("AccessToken", value);
        }
    }
    static string _baseAddress;
    static string baseAddress
    {
        get
        {
            if (string.IsNullOrEmpty(_baseAddress))
                _baseAddress = PlayerPrefs.GetString("baseAdress", "192.168.0.36");
            return _baseAddress;
        }
        set
        {
            _baseAddress = value;
            PlayerPrefs.SetString("baseAddress", value);
        }
    }
    static string _port;
    static string port
    {
        get
        {
            if (string.IsNullOrEmpty(_port))
                _port = PlayerPrefs.GetString("port", "9810");
            return _port;
        }
        set
        {
            _port = value;
            PlayerPrefs.SetString("port", value);
        }
    }

    static string authourName = "Bearer ";
    static string _facebookId;
    static string facebookId
    {
        get
        {
            if (string.IsNullOrEmpty(_facebookId))
                _facebookId = PlayerPrefs.GetString("facebookId");
            return _facebookId;
        }
        set
        {
            _facebookId = value;
            PlayerPrefs.SetString("facebookId", value);
        }
    }
    static string deviceId;
    string mailId;
    public Text textToken;

    //public bool ValidateOnServer(Product product)
    //{
    //    TcpClient tcpClient = new TcpClient();
    //    string IP = "192.168.0.36";
    //    int port = 61371;
    //    try
    //    {
    //        tcpClient.Connect(IP, port);
    //        string uri = "http://" + IP + ":" + port + "/api/pay/UpdateGold";
    //        string response = PostApi(product.receipt, uri);
    //        Debug.Log(response);
    //        m_StoreController.ConfirmPendingPurchase(product);
    //        return true;
    //    }
    //    catch (Exception)
    //    {
    //        Debug.Log("fail connect");
    //        return false;
    //    }
    //}



    public static object GetApi(string webApiUrl, string author = null, string accessToken = null)
    {
        try
        {
            // Create a WebClient to GET the request
            client = new WebClient();

            // Set the header so it knows we are sending JSON
            client.Headers[HttpRequestHeader.ContentType] = "application/json";

            if (!string.IsNullOrEmpty(author) && !string.IsNullOrEmpty(accessToken))
                //Set the header for authorization
                client.Headers["Authorization"] = author + " " + accessToken;

            string request = string.Format("http://{0}", webApiUrl);
            Debug.LogError(request);
            // Make the request
            var response = client.DownloadString(request);

            //Handler web client busy
            if (client.IsBusy)
                return false;

            // Deserialise the response into a GUID
            return JsonConvert.DeserializeObject(response);
        }
        catch (Exception ex)
        {
            Debug.Log(ex.Message);
            return false;
        }
    }

    public string PostApi(object data, string webApiUrl, string author = null, string accessToken = null)
    {
        try
        {
            if (data == null)
                data = new HttpRequest<object>();

            // Create a WebClient to POST the request
            client = new WebClient();

            // Set the header so it knows we are sending JSON
            client.Headers[HttpRequestHeader.ContentType] = "application/json";

            if (!string.IsNullOrEmpty(author) && !string.IsNullOrEmpty(accessToken))
                //Set the header for Authorization
                client.Headers["Authorization"] = author + " " + accessToken;

            // Serialise the data we are sending in to JSON
            string serialisedData = JsonConvert.SerializeObject(data);
            Debug.LogError(serialisedData);
            string request = string.Format("http://{0}", webApiUrl);
            Debug.LogError(request);
            // Make the request
            var response = client.UploadString(request, serialisedData);
            //Debug.Log(response);
            //Handler webclient busr
            if (client.IsBusy)
                return null;
            Debug.LogError(response);
            // Deserialise the response into a GUID
            //return JsonConvert.DeserializeObject(response);

            return response;
        }
        catch (Exception ex)
        {
            //Handler exception message
            Debug.Log(ex.ToString());
            return null;
        }
    }

    private static bool hasNetworkConnection()
    {
        return Application.internetReachability != NetworkReachability.NotReachable;
    }
    #endregion
}

public class PurchaseProduct
{

    public string ProductId;

    public string AppleName;


    public string GooglePlayName;
}

public class HttpRequest<DataType>
{
    public DataType Data;
}

public class HttpResponse<DataType>
{
    public ReturnCode ReturnCode;
    public string Message;
    public DataType Data;
}

public enum ReturnCode
{
    OK = 200,
    Created = 201,
    BadRequest = 400,
    TokenExpired = 401,
    NotAllowed = 403,
    NotFound = 404,
    TooManyRequests = 429,
    InternalServerError = 500,
    NotEnoughMoney = 600,
    AlreadyLinked = 601,
    LockedItem = 602,
    AmountOutOfBounds = 603,
    MailRewardInvalid = 604,
    MailRewardExpired = 605,
    MailRewardWasClaimed = 606,
    AdUnitIDInvalid = 607,
    RecentGameNotFound = 608,
    CannotGetReward = 609,
    RecentGameWasRewarded = 610,
    AdNotReady = 611,
    AlreadyExisted = 612,
    InvalidArguments = 613,
    AccountIsInvalid = 614,
    ProductPurchased = 615,
    InvalidReceipt = 616
}

public static class APIList
{
    public const string GetToken = "api/token/get?deviceID={0}&facebookID={1}";
    public const string GetAccount = "api/account/GetAccount/";
    public const string GetAccountByFacebookID = "api/account/GetAccountByFacebookID/";
    public const string UpdateAccount = "api/account/UpdateAccount/";
    public const string UpdateNickNameAndRegion = "api/account/UpdateNickNameAndRegion/";
    public const string UpdateCharacterType = "api/account/UpdateCharacterType/";
    public const string CheckExistNickName = "api/account/CheckExistNickName/";
    public const string GetLeaderboardWorld = "api/leaderboard/GetLeaderboardWorld/";
    public const string GetLeaderboardCountry = "api/leaderboard/GetLeaderboardCountry/";
    public const string GetLeaderboardFriend = "api/leaderboard/GetLeaderboardFriend/";
    public const string GetPlayerRank = "api/leaderboard/GetPlayerRank/";
    public const string LinkDeviceToFacebook = "api/facebook/LinkDeviceToFacebook/";
    public const string CheckLinkDeviceToFacebook = "api/facebook/CheckLinkDeviceToFacebook/";
    public const string GetMailNews = "api/mailbox/GetMailNews/";
    public const string GetMailReward = "api/mailbox/GetMailReward/";
    public const string ClaimMailReward = "api/mailbox/ClaimMailReward/";
    public const string ReadMailNews = "api/mailbox/ReadMailNews/";
    public const string BuyWeapon = "api/weapon/BuyWeapon/";
    public const string SetActiveWeapon = "api/weapon/SetActiveWeapon/";
    public const string UpgradeWeapon = "api/weapon/UpgradeWeapon/";
    public const string BuyEquipment = "api/armor/BuyEquipment/";
    public const string SetActiveEquipment = "api/armor/SetActiveEquipment/";
    public const string UpgradeEquipment = "api/armor/UpgradeEquipment/";
    public const string BuyHealthKit = "api/healthkit/BuyHealthKit/";
    public const string UpdateHealthKit = "api/healthkit/UpdateHealthKit/";
    public const string AddRecentGame = "api/recentgame/AddRecentGame/";
    public const string GetRecentGames = "api/recentgame/GetRecentGames/";
    public const string AdRewardFreeCrate = "api/reward/AdRewardFreeCrate/";
    public const string AdRewardEndGame = "api/reward/AdRewardEndGame/";
    public const string WinComboReward = "api/reward/WinComboReward/";
    public const string UpdateCash = "api/pay/UpdateCash/";
    public const string VerifyIAP = "api/pay/VerifyIAP/";
    public const string GetDailyQuests = "api/quest/GetDailyQuests/";
    public const string UpdateProgressDailyQuests = "api/quest/UpdateProgressDailyQuests/";
    public const string BuyPromotionPack = "api/reward/BuyPromotionPack/";
    public const string BattleResultRewad = "api/battle/reward/{userName}/{passWord}/{playerValue}/{id}";
    public const string ClaimMailPurchase = "api/mailbox/ClaimMailPurchase/";
}

public class VerifyIAPResponseData
{
    public MailRewardIAP mailReward;
}

public class VerifyIAPRequestData
{
    public ReceiptClient receipt;
    public int ItemID;
}

public class ReceiptClient
{
    public string Store;
    public string TransactionID;
    public string Payload;

    public ReceiptClient(
        string storeName,
        string transactionID,
        string payload)
    {
        this.Store = storeName;
        this.TransactionID = transactionID;
        this.Payload = payload;
    }
}

public class MailRewardIAP : MailReward
{
    public string TransactionID;
}

public class MailReward : MailNews
{
    public List<RewardItem> Reward;
}

public class MailNews
{
    public string MailObjectID;
    public string Title;
    public string Message;
    public DateTime TimeReceiveUTC;
    public TimeSpan LifeTime;
    public bool WasOpened;
}

public class RewardItem
{
    public RewardType RewardType;
    public int ItemId;
    public int QuantityMin;
    public int QuantityMax;

    public int Quantity;

    public RewardItem(RewardType rewardType, int itemID, int quantityMin, int quantityMax)
    {
        RewardType = rewardType;
        ItemId = itemID;
        QuantityMin = quantityMin;
        QuantityMax = quantityMax;
    }
}

public enum RewardType : byte
{
    Weapon = 1,
    Armor,
    Currency,
    QuestItem
}

public class ClaimRewardsResponseData
{
    /// <summary>
    /// là số lượng sau khi đã roll random trên server, trả về cùng với ReturnCode OK
    /// </summary>
    public List<int> QuantityReward;

    public ClaimRewardsResponseData() { }

    public ClaimRewardsResponseData(List<RewardItem> reward)
    {
        if (reward != null)
        {
            QuantityReward = new List<int>();
            foreach (RewardItem r in reward)
            {
                QuantityReward.Add(r.Quantity);
            }
        }
    }
}

public class ClaimMailRewardRequestData
{
    public string MailObjectID;
}

